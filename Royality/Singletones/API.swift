//
//  API.swift
//  Royality
//
//  Created by Дмитрий Жданов on 11/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import Foundation

class API {
    static let shared = API()
    private var data: Data!
    //private let baseURL = "http://91.243.97.74:1155/Api"
    private let baseURL = "https://loyalty.nppltt.ru:5543/Api"
    
    func getCordaUsers(port: String) -> Data {
        //let params = ""
        //configRequest(url: "http://51.145.142.95:\(port)/api/example/get-user", method: "GET", params: [String: String]())
        return data
    }
    
    func login(email: String, password: String) -> Data {
        let params = ["Email": email, "Password": password]
        configRequest(url: "/Login", method: "POST", token: nil, params: params)
        return data
    }
    
    func registration(name: String, email: String, password: String) -> Data {
        let params = ["Name": name, "Email": email, "Password": password]
        configRequest(url: "/Registration", method: "POST", token: nil, params: params)
        return data
    }
    
    func forgotPassword(email: String) -> Data {
        let params = ["Email": email]
        configRequest(url: "/PasswordForgot", method: "POST", token: nil, params: params)
        return data
    }
    
    func getReferenceBooks(token: String) -> Data {
        configRequest(url: "/GetReferenceBooks", method: "GET", token: token, params: nil)
        return data
    }
    
    func getWalletInfo(token: String) -> Data {
        configRequest(url: "/GetWalletInfoV2", method: "GET", token: token, params: nil)
        return data
    }
    
    func getUserInfo(token: String) -> Data {
        configRequest(url: "/GetUserInfo", method: "GET", token: token, params: nil)
        return data
    }
    
    func getAllShopsLight(token: String) -> Data {
        configRequest(url: "/GetAllShopsLightV2", method: "GET", token: token, params: nil)
        return data
    }
    
    func getCategories(token: String) -> Data {
        configRequest(url: "/GetCategories", method: "GET", token: token, params: nil)
        return data
    }
    
    func getCompanyInfo(token: String, companyId: Int) -> Data {
        configRequest(url: "/GetCompanyInfo?companyId=\(companyId)", method: "GET", token: token, params: nil)
        return data
    }
    
    func getCompanyInfoByShopId(token: String, shopId: Int) -> Data {
        configRequest(url: "/GetCompanyInfoByShopIdV2?shopId=\(shopId)", method: "GET", token: token, params: nil)
        return data
    }
    
    func getAllPromotionsLight(token: String) -> Data {
        configRequest(url: "/GetAllPromotionsLightV2", method: "GET", token: token, params: nil)
        return data
    }
    
    func getPromotionInfo(token: String, promoId: Int) -> Data {
        configRequest(url: "/GetPromotionInfoV2?promotionId=\(promoId)", method: "GET", token: token, params: nil)
        return data
    }
    
    func getAllCompanyPromotions(token: String, companyId: Int) -> Data {
        configRequest(url: "/GetAllCompanyPromotionsV2?companyId=\(companyId)", method: "GET", token: token, params: nil)
        return data
    }
    
    /*func updateUserInfo(token: String, name: String) -> Data {
        let params = ["firstName": name, "gender": 0, "phone": "string", "birthdate": "2018-11-30T11:16:28.405Z"] as [String : Any]
        configRequest(url: "/UpdateUserInfo", method: "POST", token: token, params: params)
        return data
    }*/
    
    fileprivate func configRequest(url: String, method: String, token: String?, params: [String: Any]?) {
        guard let url = URL(string: "\(baseURL)\(url)") else {return}
        
        //print(url)
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        
        switch method {
        case "POST":
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params!, options:[]) else {return}
            request.httpBody = httpBody
            //print(JSONSerialization.jsonObject(with: httpBody, options: []) ?? "empty json")
        default:
            break
        }
        if let token = token {
            request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        }
        sendRequest(request: request)
    }
    
    fileprivate func sendRequest(request: URLRequest) {
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            //if let response = response {print(response)}
            guard let data = data else {
                self.data = "{\"answer\": \"connection_error\"}".data(using: .utf8)
                semaphore.signal()
                return
            }
            self.data = data
            //printJson(data: data)
            
            semaphore.signal()
            }.resume()
        semaphore.wait()
    }
}
