//
//  Utils.swift
//  Royality
//
//  Created by Дмитрий Жданов on 12/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import Foundation
import UIKit

func presentAllert(VC: UIViewController, message: String) {
    DispatchQueue.main.async {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(alertAction)
        VC.present(alertVC, animated: true, completion: nil)
    }
}

func getAppDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

func getMainNavVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavVC")
}

func getRegVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegVC")
}

func getTabBarVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC")
}

func getShopVC() -> ShopViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShopVC") as! ShopViewController
}

func getShopInfoVC() -> ShopInfoController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShopInfoVC") as! ShopInfoController
}

func getMapVC() -> MapViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapViewController
}

func getPromoVC() -> PromoCollectionViewController  {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PromoVC") as! PromoCollectionViewController
}

func getPromoInfoVC() -> PromoInfoController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PromoInfoVC") as! PromoInfoController
}

func getQrCodeVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QrCodeVC")
}

func printJson(data: Data!) {
    print(try! JSONSerialization.jsonObject(with: data, options: []))
}

func imageFromBase64(base64: String) -> UIImage {
    return UIImage(data: Data(base64Encoded: base64)!)!
}
