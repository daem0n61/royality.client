//
//  Model.swift
//  Royality
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int
    let name, imageUrl: String
}

struct Categories: Codable {
    let result: [Category]
}

struct BonusProgram: Codable {
    let /*bonusProgramId,*/ maxPayByBonusLimit, valuePercent: Int
}

struct SocialNetwork: Codable {
    let type: Int
    let baseUrl: String
}

struct SocialNetworkLinks: Codable {
    let companyId: Int
    let urlAddress: String
    let socialNetworl: SocialNetwork
}

struct Shop: Codable {
    let shopId/*, companyId*/: Int
    let shopName, address, phone, openingHours: String
    let latitude, longitude: Double
}

struct Company: Codable {
    let companyId: Int
    let balance: Double
    let companyName, description, category: String
    let circleImage, squareImage/*, coalitionName*/: String?
    let bonusProgram: BonusProgram
    let shops: [Shop]
}

struct CompanyAnswer: Codable {
    let result: Company
}

struct Model: Codable {
    let companies: [Company]?
    let walletId: String
}

struct LightShop: Codable {
    let shopId, categoryId/*, companyId*/: Int
    let name, address: String
    let logoUrl: String?
    let latitude, longitude: Double
}

struct LightModel: Codable {
    let result: [LightShop]
}

struct CompanyInfo: Codable {
    let result: Company
}

struct User: Codable {
    let firstName: String
}

struct Promo: Codable {
    let name, smallImagePath, companyName: String
    let promotionId: Int
}

struct PromoModel: Codable {
    let result: [Promo]
}

struct PromoInfo: Codable {
    let name, bigImagePath, description, companyName: String
    let companyId: Int
}

struct PromoInfoModel: Codable {
    let result: PromoInfo
}

struct UserInfo: Codable {
    let userInfo: User
}

struct ExtShop {
    let name, address, description, category, phone, openingHours: String
    let circleImage, squareImage: String?
    let latitude, longitude: Double
    let bonusProgram: BonusProgram
}

/*struct Coalition {
    let name, description, discount: String
    let circleImageName, squareImageName: String
    let shops:[Shop]
}

struct ShopWithBonus {
    let shop: Shop
    let bonus: String
}

struct CoalitionWithBonus {
    let coalition: Coalition
    let bonus: String
}*/
