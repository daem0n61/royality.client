//
//  CoalitionTableViewCell.swift
//  Royality
//
//  Created by Дмитрий Жданов on 15/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class CoalitionTableViewCell: UITableViewCell {
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var discountTextField: UITextView!
    @IBOutlet weak var header: UITextView!
    @IBOutlet weak var shopsListText: UITextView!
    @IBOutlet weak var bonusText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /*func fill(item: Coalition, bonus: String) {
        picture.image = UIImage(named: item.circleImageName)
        bonusText.text = bonus
        discountTextField.text = item.discount
        header.text = "\"\(item.name)\""
        shopsListText.text = ""
        for shop in item.shops {
            shopsListText.text += "\(shop.name)\n"
        }
    }*/
}
