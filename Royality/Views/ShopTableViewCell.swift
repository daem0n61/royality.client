//
//  ShopTableViewCell.swift
//  Royality
//
//  Created by Дмитрий Жданов on 13/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextView!
    @IBOutlet weak var balanceTextView: UITextView!
    @IBOutlet weak var discountTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(company: Company) {
        DispatchQueue.global(qos: .userInteractive).async {
            if company.circleImage != nil {
                if let url = URL(string: company.circleImage!) {
                    let data = try! Data(contentsOf: url)
                    let image = UIImage(data: data)
                    
                    DispatchQueue.main.async {
                        self.logoImageView.image = image
                    }
                }
            }
        }
        
        discountTextView.text = "Скидка до \(company.bonusProgram.maxPayByBonusLimit)%"
        nameTextField.text = company.companyName
        balanceTextView.text = String(describing: Int(company.balance))
    }
}
