//
//  OfferCollectionViewCell.swift
//  Royality
//
//  Created by Дмитрий Жданов on 31/12/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var nameTextView: UITextView!
    
    func fill(item: Promo) {
        DispatchQueue.global(qos: .userInteractive).async {
            //if item.imageUrl != nil {
            if let url = URL(string: item.smallImagePath) {
                let data = try! Data(contentsOf: url)
                let image = UIImage(data: data)
                
                DispatchQueue.main.async {
                    self.imageView.image = image
                }
            }
            //}
        }
        textView.text = item.name
        nameTextView.text = item.companyName
    }
}
