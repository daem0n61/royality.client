//
//  CategoryCollectionViewCell.swift
//  Royality
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UITextView!
    
    func fill(item: Category) {
        DispatchQueue.global(qos: .userInteractive).async {
            //if item.imageUrl != nil {
                if let url = URL(string: item.imageUrl) {
                    let data = try! Data(contentsOf: url)
                    let image = UIImage(data: data)
                    
                    DispatchQueue.main.async {
                        self.image.image = image
                    }
                }
            //}
        }
        title.text = item.name
    }
}
