//
//  MapViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 13/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var shops: [Shop]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var annotations = [MKPointAnnotation]()
        for shop in shops {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: shop.latitude, longitude: shop.longitude)
            annotation.title = shop.shopName
            annotation.subtitle = shop.address
            annotations.append(annotation)
        }
        mapView.showAnnotations(annotations, animated: true)
    }
}
