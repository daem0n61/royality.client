//
//  BonusViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 11/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class BonusViewController: UIViewController {
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var refresh = UIRefreshControl()
    
    var shops = [ShopWithBonus]()
    //var coalitions = [CoalitionWithBonus]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        tableView.addSubview(refresh)
        refreshHandler()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //refreshHandler()
    }
    
    @objc func refreshHandler() {
        shops.removeAll()
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getWalletInfo(token: token)
            do {
                let parsed = try JSONDecoder().decode(TakenModel.self, from: data)
                
                for company in parsed.companies! {
                    let shops = company.shops
                    var address = String()
                    for shop in shops {
                        address.append(contentsOf: "Адрес: \(shop.address)\nГрафик работы: \(shop.openingHours)\nТелефон: \(shop.phone)\n\n")
                    }
                    let shop = shops[0]
                    var name = String()
                    if shops.count > 1 {
                        name = company.companyName
                    } else {
                        name = shop.shopName
                    }
                    
                    self.shops.append(ShopWithBonus(shop: Shop(name: name, address: address, description: company.description, discount: "\(company.bonusProgram.payByBonusLimit)%", category: company.category, circleImage: company.circleImage, squareImage: company.squareImage, latitude: shop.latitude, longitude: shop.longitude), bonus: "\(company.balance)"))
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
            }
        }
        refresh.endRefreshing()
    }
    
    @IBAction func segmentSwitched(_ sender: Any) {
        tableView.reloadData()
    }
}

extension BonusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if segment.selectedSegmentIndex == 0 {
            return shops.count > 0 ? shops.count : 1
        /*} else {
            return coalitions.count
        }*/
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //if segment.selectedSegmentIndex == 0 {
            if shops.count > 0 {
                if let itemCell = tableView.dequeueReusableCell(withIdentifier: "ShopCell", for: indexPath) as? ShopTableViewCell {
                    itemCell.fill(item: shops[indexPath.row].shop, bonus: shops[indexPath.row].bonus)
                    return itemCell
                }
            } else {
                let itemCell = UITableViewCell()
                itemCell.textLabel?.text = "Ваш список бонусов пуст"
                itemCell.textLabel?.textAlignment = .center
                return itemCell
            }
        /*}
        if segment.selectedSegmentIndex == 1 {
            /*if let itemCell = tableView.dequeueReusableCell(withIdentifier: "CoalitionCell", for: indexPath) as? CoalitionTableViewCell {
                itemCell.fill(item: coalitions[indexPath.row].coalition, bonus: coalitions[indexPath.row].bonus)
                return itemCell
            }*/
            let itemCell = UITableViewCell()
            itemCell.textLabel?.text = "Пока не создано ни одной коалиции"
            itemCell.textLabel?.textAlignment = .center
            return itemCell
        }*/
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if (segment.selectedSegmentIndex == 0) {
            let VC = getShopVC()
            VC.shop = shops[indexPath.row].shop
            VC.bonus = shops[indexPath.row].bonus
            self.navigationController?.pushViewController(VC, animated: true)
        /*} else {
            let send = coalitions[indexPath.row]
            performSegue(withIdentifier: "BonusToCoalition", sender: send)
        }*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //if (segment.selectedSegmentIndex == 0) {
            return shops.count > 0 ? 170 : 50
        /*} else {
            return 50
        }*/
    }
}
