//
//  CabinetViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 15/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class CabinetViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextView!
    @IBOutlet weak var emailTextField: UITextView!
    @IBOutlet weak var cityTextField: UITextView!
    @IBOutlet weak var container: UIContentContainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (children[0] as! MenuViewController).delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let token = getAppDelegate().token!
        
        /*DispatchQueue.global(qos: .utility).async {
            let data = API.shared.updateUserInfo(token: token, name: "Дмитрий")
            print(try! JSONSerialization.jsonObject(with: data, options: []) ?? "empty json")
        }*/
        
        DispatchQueue.global(qos: .utility).async {
            let data = API.shared.getUserInfo(token: token)
            do {
                let parsed = try JSONDecoder().decode(UserInfo.self, from: data)
                let name = parsed.userInfo.firstName
                DispatchQueue.main.async {
                    self.nameTextField.text = name
                }
            } catch {
                print(error)
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
        }
        
        if let loginDate = UserDefaults.standard.stringArray(forKey: "LoginData") {
            emailTextField.text = loginDate[0]
        }
    }
    
    @IBAction func promoButtonPressed(_ sender: Any) {
        let alertVC = UIAlertController(title: "Введите ваш промокод", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        let sendPromo = UIAlertAction(title: "Отправить", style: .default) { (action) in
            guard let promoCode = alertVC.textFields?[0].text else {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
            
            if promoCode == "" {
                presentAllert(VC: self, message: "Введена пустая почта")
                return
            }
            
            /*let data = API.shared.forgotPassword(email: email)
            let parsed = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            print(parsed)
            if let error = parsed["error"] as? [String: AnyObject] {
                if let message = error["message"] as? String {
                    presentAllert(VC: self, message: message)
                    return
                }
            }*/
        }
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Промокод"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(sendPromo)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func contactUsButtonPressed(_ sender: Any) {
        let alertVC = UIAlertController(title: nil, message: "У вас есть предложения или вопросы по нашему приложению?\nВы можете связаться с нами:\nroyalitysup@gmail.com\nили ВКонтакте", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        let openVk = UIAlertAction(title: "Открыть ВКонтакте", style: .default) { (action) in
            UIApplication.shared.openURL(URL(string: "https://vk.com/alexalper")!)
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(openVk)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        let alertVC = UIAlertController(title: nil, message: "Вы точно хотите выйти?", preferredStyle: .alert)
        let no = UIAlertAction(title: "Нет", style: .cancel)
        let yes = UIAlertAction(title: "Да", style: .destructive) { (action) in
            UserDefaults.standard.removeObject(forKey: "Token")
            UserDefaults.standard.removeObject(forKey: "WalletId")
            self.performSegue(withIdentifier: "CabinetToLogin", sender: nil)
        }
        alertVC.addAction(no)
        alertVC.addAction(yes)
        self.present(alertVC, animated: true, completion: nil)
    }
}
