//
//  PromoCollectionViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 30/12/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class PromoCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var promos = [Promo]()
    var companyId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            var data = Data()
            if let companyId = self.companyId {
                data = API.shared.getAllCompanyPromotions(token: token, companyId: companyId)
            } else {
                data = API.shared.getAllPromotionsLight(token: token)
            }
            
            do {
                let parsed = try JSONDecoder().decode(PromoModel.self, from: data)
                self.promos = parsed.result
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromoCell", for: indexPath) as? PromoCollectionViewCell {
            cell.fill(item: promos[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width / 2 - 10
        let height = width * 3 / 4
        return CGSize(width: width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let VC = getPromoInfoVC()
        VC.promoId = promos[indexPath.row].promotionId
        self.navigationController?.pushViewController(VC, animated: true)
        //print(promo[indexPath.row])
    }
}
