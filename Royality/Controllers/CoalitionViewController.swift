//
//  CoalitionViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 15/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class CoalitionViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var discountText: UITextView!
    @IBOutlet weak var bonusText: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    /*var coalition: Coalition!
    var bonus: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = coalition.name
        image.image = UIImage(named: coalition.squareImageName)
        discountText.text = coalition.discount
        bonusText.text = bonus
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CoalitionToShop" {
            if let vc = segue.destination as? ShopViewController {
                let send = sender as? ShopWithBonus
                vc.shop = send?.shop
                vc.bonus = send?.bonus
            }
        }
        if segue.identifier == "CoalitionToMap" {
            if let vc = segue.destination as? MapViewController {
                let send = sender as? [Shop]
                vc.shops = send
            }
        }
    }
    
    @IBAction func showOnMapTapped(_ sender: Any) {
        var send = [Shop]()
        for shop in coalition.shops {
            send.append(shop)
        }
        performSegue(withIdentifier: "CoalitionToMap", sender: send)
    }*/
}

/*extension CoalitionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coalition.shops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = coalition.shops[indexPath.row].name
        //cell.imageView?.image = UIImage(named: coalition.shops[indexPath.row].circleImageName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let send = ShopWithBonus(shop: coalition.shops[indexPath.row], bonus: bonus)
        performSegue(withIdentifier: "CoalitionToShop", sender: send)
    }
}*/
