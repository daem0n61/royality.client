//
//  BonusTableViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 06/12/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class BonusTableViewController: UITableViewController {
    var companies = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        refreshHandler()
    }
    
    @objc func refreshHandler() {
        companies.removeAll()
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getWalletInfo(token: token)
            do {
                let parsed = try JSONDecoder().decode(Model.self, from: data)
                self.companies = parsed.companies!
                
                /*for company in parsed.companies! {
                    let shops = company.shops
                    let shop = shops[0]
                    var name = String()
                    if shops.count > 1 {
                        name = company.companyName
                    } else {
                        name = shop.shopName
                    }
                }*/
                
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
            }
        }
    }
    
    /*override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }*/
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count > 0 ? companies.count : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if companies.count > 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell", for: indexPath) as? ShopTableViewCell {
                cell.fill(company: companies[indexPath.row])
                return cell
            }
            return UITableViewCell()
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.text = "Ваш список бонусов пуст"
            cell.textLabel?.textAlignment = .center
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = getShopVC()
        VC.companyId = companies[indexPath.row].companyId
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return companies.count > 0 ? 100 : 50
    }
}
