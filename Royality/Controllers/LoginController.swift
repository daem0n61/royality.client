//
//  LoginViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 11/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var oflineQrButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: "WalletId") == nil {
            oflineQrButton.isEnabled = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSizeFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        scrollView.contentOffset = CGPoint(x: 0, y: keyboardSizeFrame.height)
    }
    
    @objc fileprivate func keyboardWillHide() {
        scrollView.contentOffset = CGPoint.zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        let login = loginTextField.text!
        let password = passwordTextField.text!
        
        if (login == "" || password == "") {
            presentAllert(VC: self, message: "Заполните пустые поля")
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let data = API.shared.login(email: login, password: password)
            let parsed = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            
            if let answer = parsed["answer"] as? String {
                if answer == "connection_error" {
                    presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                    return
                }
            }
                
            if let error = parsed["error"] as? [String: AnyObject] {
                if let message = error["message"] as? String {
                    presentAllert(VC: self, message: message)
                    return
                }
            }
            
            UserDefaults.standard.set(parsed["token"] as! String, forKey: "Token")
            UserDefaults.standard.set(parsed["walletId"] as! String, forKey: "WalletId")
            
            DispatchQueue.main.async {
                getAppDelegate().token = (parsed["token"] as! String)
                self.performSegue(withIdentifier: "LoginToTab", sender: nil)
            }
        }
    }
    
    @IBAction func registrationButtonTapped(_ sender: Any) {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        let VC = getRegVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        let alertVC = UIAlertController(title: "Восстановление пароля", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        let restoreAlert = UIAlertAction(title: "Восстановить", style: .default) { (action) in
            guard let email = alertVC.textFields?[0].text else {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
            
            if email == "" {
                presentAllert(VC: self, message: "Введена пустая почта")
                return
            }
            
            
            let data = API.shared.forgotPassword(email: email)
            let parsed = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            print(parsed)
            if let error = parsed["error"] as? [String: AnyObject] {
                if let message = error["message"] as? String {
                    presentAllert(VC: self, message: message)
                    return
                }
            }
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Введите свою почту"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(restoreAlert)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func oflineQrButtonTapped(_ sender: Any) {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        let VC = getQrCodeVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
