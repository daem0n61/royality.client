//
//  EndTutorialViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 06/12/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class EndTutorialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func EndTutorialButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "FirstLaunch")
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
