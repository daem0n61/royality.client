//
//  ShopInfoController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 02/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class ShopInfoController: UIViewController {
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var openingHoursTextView: UITextView!
    
    var shop: Shop!
    var delegate: ShopViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        callButton.setTitle(shop.phone, for: .normal)
        addressTextView.text = shop.address
        openingHoursTextView.text = shop.openingHours
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //let frame = delegate.bottomView.frame
        //delegate.bottomView.heightAnchor.constraint(equalTo: height, constant: 0)
        //delegate.containerView.
        //delegate.view.layoutIfNeeded()
    }
    
    @IBAction func callButtonTapped(_ sender: Any) {
        let phone = shop.phone.replacingOccurrences(of: " ", with: "")
         
         if let url = URL(string: "tel://\(phone)") {
         UIApplication.shared.openURL(url)
         }
    }
}
