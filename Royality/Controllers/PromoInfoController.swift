//
//  PromoInfoController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 29/01/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class PromoInfoController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    var promoId, companyId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getPromotionInfo(token: token, promoId: self.promoId)
            do {
                let parsed = try JSONDecoder().decode(PromoInfoModel.self, from: data)
                self.companyId = parsed.result.companyId
                let item = parsed.result
                DispatchQueue.main.async {
                    self.title = item.companyName
                    self.descriptionTextView.text = item.description
                }
                if let url = URL(string: item.bigImagePath) {
                    let data = try! Data(contentsOf: url)
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data: data)
                    }
                }
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
        }
    }
    
    @IBAction func openShopButtonTapped(_ sender: Any) {
        let VC = getShopVC()
        VC.companyId = companyId
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
