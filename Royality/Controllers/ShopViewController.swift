//
//  ShopScreenViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 13/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class ShopViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var topViewClone: UIView!
    @IBOutlet weak var cashbackTextViewClone: UITextView!
    @IBOutlet weak var discountTextViewClone: UITextView!
    @IBOutlet weak var bonusTextViewClone: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cashbackTextView: UITextView!
    @IBOutlet weak var discountTextView: UITextView!
    @IBOutlet weak var bonusTextView: UITextView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var companyId: Int!
    var shops = [Shop]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Map"), style: .plain, target: self, action: #selector(showOnMapTapped))
        topViewClone.isHidden = true
        
        topViewClone.layer.shadowColor = UIColor.darkGray.cgColor
        topViewClone.layer.shadowOpacity = 1
        topViewClone.layer.shadowOffset = CGSize.zero
        topViewClone.layer.shadowRadius = 5
        
        bottomView.layer.shadowColor = UIColor.darkGray.cgColor
        bottomView.layer.shadowOpacity = 1
        bottomView.layer.shadowOffset = CGSize.zero
        bottomView.layer.shadowRadius = 5
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        //bottomView.bottomAnchor
        
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getCompanyInfo(token: token, companyId: self.companyId)
            do {
                let parsed = try JSONDecoder().decode(CompanyAnswer.self, from: data)
                let company = parsed.result
                DispatchQueue.main.async {
                    (self.children[0] as! ShopInfoPageController).initialize(shops: parsed.result.shops, delegate: self)
                    self.title = company.companyName
                    self.cashbackTextView.text = String(describing: company.bonusProgram.valuePercent)
                    self.discountTextView.text = String(describing: company.bonusProgram.maxPayByBonusLimit) + "%"
                    self.bonusTextView.text = String(describing: Int(company.balance))
                    self.descriptionTextView.text = company.description
                    self.cashbackTextViewClone.text = String(describing: company.bonusProgram.valuePercent)
                    self.discountTextViewClone.text = String(describing: company.bonusProgram.maxPayByBonusLimit) + "%"
                    self.bonusTextViewClone.text = String(describing: Int(company.balance))
                }
                if company.squareImage != nil {
                    if let url = URL(string: company.squareImage!) {
                        let data = try! Data(contentsOf: url)
                        DispatchQueue.main.async {
                            self.mainImageView.image = UIImage(data: data)
                        }
                    }
                }
                self.shops = parsed.result.shops
                
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
        }
    }
    
    @objc fileprivate func showOnMapTapped() {
        let VC = getMapVC()
        VC.shops = shops
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func showPromoButtonTapped(_ sender: Any) {
        let VC = getPromoVC()
        VC.companyId = companyId
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > topView.frame.minY {
            topViewClone.isHidden = false
        } else {
            topViewClone.isHidden = true
        }
    }
}
