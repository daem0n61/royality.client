//
//  RegistrationViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 14/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSizeFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        scrollView.contentOffset = CGPoint(x: 0, y: keyboardSizeFrame.height)
    }
    
    @objc fileprivate func keyboardWillHide() {
        scrollView.contentOffset = CGPoint.zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func registrationButtonTapped(_ sender: Any) {
        
        var texts = [String]()
        for field in textFields {
            if field.text == "" {
                presentAllert(VC: self, message: "Заполните пустые поля")
                return
            }
            texts.append(field.text!)
        }
        
        for field in textFields {
            field.resignFirstResponder()
        }
        
        if textFields[2].text != textFields[3].text {
            presentAllert(VC: self, message: "Пароли не совпадают")
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let data = API.shared.registration(name: texts[0], email: texts[1], password: texts[2])
            let parsed = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            
            if let error = parsed["error"] as? [String: AnyObject] {
                if let message = error["message"] as? String {
                    presentAllert(VC: self, message: message)
                    return
                }
            }
            
            let walletId = (parsed["walletInfo"] as! [String: AnyObject])["walletId"] as! String
            UserDefaults.standard.set([texts[1], texts[2]], forKey: "LoginData")
            UserDefaults.standard.set(walletId, forKey: "WalletId")
            
            DispatchQueue.main.async {
                getAppDelegate().token = (parsed["token"] as! String)
                getAppDelegate().window?.rootViewController = getTabBarVC()
            }
        }
    }
}
