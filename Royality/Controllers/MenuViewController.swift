//
//  MenuViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 02/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {
    struct MenuItem {
        let title: String
        let image: UIImage
    }
    
    var delegate: UIViewController!
    
    let menuItems = [MenuItem(title: "Как пользоваться?", image: #imageLiteral(resourceName: "Tutorial")),
                  MenuItem(title: "Кошелек", image: #imageLiteral(resourceName: "QrCode")),
                  MenuItem(title: "Ввести промокод", image: #imageLiteral(resourceName: "PromoCode")),
                  MenuItem(title: "Контакты разработчиков", image: #imageLiteral(resourceName: "ContactUs")),
                  MenuItem(title: "Выйти", image: #imageLiteral(resourceName: "LogOut"))]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)
        /*cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOpacity = 1
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shadowRadius = 5*/
        cell.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
        cell.layer.borderWidth = 1
        //cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = menuItems[indexPath.section].title
        cell.imageView?.image = menuItems[indexPath.section].image
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            delegate.performSegue(withIdentifier: "CabinetToTutorial", sender: nil)
        case 1:
            delegate.navigationController?.pushViewController(getQrCodeVC(), animated: true)
        case 2:
            let alertVC = UIAlertController(title: "Введите ваш промокод", message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
            let sendPromo = UIAlertAction(title: "Отправить", style: .default) { (action) in
                guard let promoCode = alertVC.textFields?[0].text else {
                    presentAllert(VC: self.delegate, message: "Что-то пошло не так, попробуйте еще раз")
                    return
                }
                
                if promoCode == "" {
                    presentAllert(VC: self.delegate, message: "Введена пустая почта")
                    return
                }
                
                /*let data = API.shared.forgotPassword(email: email)
                 let parsed = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                 print(parsed)
                 if let error = parsed["error"] as? [String: AnyObject] {
                 if let message = error["message"] as? String {
                 presentAllert(VC: self, message: message)
                 return
                 }
                 }*/
            }
            
            alertVC.addTextField { (textField) in
                textField.placeholder = "Промокод"
                textField.keyboardType = UIKeyboardType.emailAddress
            }
            alertVC.addAction(cancelAction)
            alertVC.addAction(sendPromo)
            delegate.present(alertVC, animated: true, completion: nil)
        case 3:
            let alertVC = UIAlertController(title: nil, message: "У вас есть предложения или вопросы по нашему приложению?\nВы можете связаться с нами:\nroyalitysup@gmail.com\nили ВКонтакте", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
            let openVk = UIAlertAction(title: "Открыть ВКонтакте", style: .default) { (action) in
                UIApplication.shared.openURL(URL(string: "https://vk.com/alexalper")!)
            }
            alertVC.addAction(cancelAction)
            alertVC.addAction(openVk)
            delegate.present(alertVC, animated: true, completion: nil)
        case 4:
            let alertVC = UIAlertController(title: nil, message: "Вы точно хотите выйти?", preferredStyle: .alert)
            let no = UIAlertAction(title: "Нет", style: .cancel)
            let yes = UIAlertAction(title: "Да", style: .destructive) { (action) in
                UserDefaults.standard.removeObject(forKey: "Token")
                UserDefaults.standard.removeObject(forKey: "WalletId")
                self.delegate.performSegue(withIdentifier: "CabinetToLogin", sender: nil)
            }
            alertVC.addAction(no)
            alertVC.addAction(yes)
            delegate.present(alertVC, animated: true, completion: nil)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
