//
//  HomeViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 09/10/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import MapKit
import CoreLocation

class HomeViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailLogoImageView: UIImageView!
    @IBOutlet weak var detailNameTextView: UITextView!
    @IBOutlet weak var detailAddressTextView: UITextView!
    @IBOutlet weak var detailOpeningHoursTextView: UITextView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    var shops = [LightShop]()
    var categories = [Category]()
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D!
    var mapOriginHeight = CGFloat()
    var isMapFullscreen = false
    var firstStart = true
    var isDetailShow = false
    var detailedCompany: Company!
    var detailedShop: Shop!
    var topAnchor, bottomAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways: break
            case .authorizedWhenInUse: break
            case .denied: locationManager.requestWhenInUseAuthorization()
            case .notDetermined: locationManager.requestWhenInUseAuthorization()
            case .restricted: locationManager.requestWhenInUseAuthorization()
        }
        
        if !UserDefaults.standard.bool(forKey: "FirstLaunch") {
            performSegue(withIdentifier: "HomeToTutorial", sender: nil)
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userLocation.title = "Вы здесь!"
        
        detailView.translatesAutoresizingMaskIntoConstraints = false
        topAnchor = detailView.topAnchor.constraint(equalTo: view.bottomAnchor)
        topAnchor?.isActive = true
        bottomAnchor = detailView.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -10)
        bottomAnchor?.isActive = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mapOriginHeight = mapView.frame.height
        getMapAnnotations()
    }
    
    fileprivate func getMapAnnotations() {
        let token = getAppDelegate().token!
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getAllShopsLight(token: token)
            do {
                let parsed = try JSONDecoder().decode(LightModel.self, from: data)
                self.shops.removeAll()
                self.shops = parsed.result
                
                var annotations = [MKPointAnnotation]()
                for shop in self.shops {
                    let annotation = MKPointAnnotation()
                    annotation.title = shop.name
                    annotation.subtitle = shop.address
                    annotation.coordinate = CLLocationCoordinate2D(latitude: shop.latitude, longitude: shop.longitude)
                    annotations.append(annotation)
                }
                DispatchQueue.main.async {
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.mapView.addAnnotations(annotations)
                }
            } catch {
                print(error)
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
        }
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getCategories(token: token)
            do {
                let parsed = try JSONDecoder().decode(Categories.self, from: data)
                self.categories = parsed.result
            } catch {
                print(error)
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
            DispatchQueue.main.async {
                self.categoryCollectionView.reloadData()
            }
        }
    }
    
    fileprivate func updateMapAnnotations(categoryId: Int) {
        if categoryId == 0 {
            var annotations = [MKPointAnnotation]()
            for shop in shops {
                let annotation = MKPointAnnotation()
                annotation.title = shop.name
                annotation.subtitle = shop.address
                annotation.coordinate = CLLocationCoordinate2D(latitude: shop.latitude, longitude: shop.longitude)
                annotations.append(annotation)
            }
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.showAnnotations(annotations, animated: true)
        } else {
            DispatchQueue.global(qos: .userInteractive).async {
                var shops = [LightShop]()
                for shop in self.shops {
                    if shop.categoryId == categoryId {
                        shops.append(shop)
                    }
                }
                var annotations = [MKPointAnnotation]()
                for shop in shops {
                    let annotation = MKPointAnnotation()
                    annotation.title = shop.name
                    annotation.subtitle = shop.address
                    annotation.coordinate = CLLocationCoordinate2D(latitude: shop.latitude, longitude: shop.longitude)
                    annotations.append(annotation)
                }
                DispatchQueue.main.async {
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.mapView.showAnnotations(annotations, animated: true)
                }
            }
        }
    }
    
    fileprivate func showDetail(coordinate: CLLocationCoordinate2D) {
        let token = getAppDelegate().token!
        self.detailView.isHidden = false
        if isDetailShow {
            topAnchor?.isActive = true
            bottomAnchor?.isActive = false
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.detailLogoImageView.image = nil
                self.detailNameTextView.text = nil
                self.detailAddressTextView.text = nil
                self.detailOpeningHoursTextView.text = nil
                DispatchQueue.global(qos: .userInteractive).async {
                    for shop in self.shops {
                        if (shop.latitude == coordinate.latitude && shop.longitude == coordinate.longitude) {
                            let data = API.shared.getCompanyInfoByShopId(token: token, shopId: shop.shopId)
                            do {
                                let parsed = try JSONDecoder().decode(CompanyInfo.self, from: data)
                                let company = parsed.result
                                let shops = company.shops
                                if shops.count > 1 {
                                    for shop in shops {
                                        if (shop.latitude == coordinate.latitude && shop.longitude == coordinate.longitude) {
                                            self.detailedShop = shop
                                            // self.detailedShop = ShopWithBonus(shop: Shop(name: shop.shopName, address: shop.address, description: company.description, category: company.category, phone: shop.phone, openingHours: shop.openingHours, circleImage: company.circleImage, squareImage: company.squareImage, latitude: shop.latitude, longitude: shop.longitude, bonusProgram: company.bonusProgram), bonus: "\(Int(parsed.result.balance))")
                                            
                                            break
                                        }
                                    }
                                } else {
                                    self.detailedShop = shops[0]
                                }
                                self.filldetail()
                            } catch {
                                print(error)
                                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                                return
                            }
                            break
                        }
                    }
                }
                
                self.topAnchor?.isActive = false
                self.bottomAnchor?.isActive = true
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        } else {
            detailView.frame = CGRect(x: 10, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width - 20, height: self.detailView.frame.height)
            DispatchQueue.global(qos: .userInteractive).async {
                for shop in self.shops {
                    if (shop.latitude == coordinate.latitude && shop.longitude == coordinate.longitude) {
                        let data = API.shared.getCompanyInfoByShopId(token: token, shopId: shop.shopId)
                        do {
                            let parsed = try JSONDecoder().decode(CompanyInfo.self, from: data)
                            let company = parsed.result
                            let shops = company.shops
                            self.detailedCompany = company
                            if shops.count > 1 {
                                for shop in shops {
                                    if (shop.latitude == coordinate.latitude && shop.longitude == coordinate.longitude) {
                                        self.detailedShop = shop
                                        // self.detailedShop = ShopWithBonus(shop: Shop(name: shop.shopName, address: shop.address, description: company.description, category: company.category, phone: shop.phone, openingHours: shop.openingHours, circleImage: company.circleImage, squareImage: company.squareImage, latitude: shop.latitude, longitude: shop.longitude, bonusProgram: company.bonusProgram), bonus: "\(Int(parsed.result.balance))")
                                        
                                        break
                                    }
                                }
                            } else {
                                self.detailedShop = shops[0]
                            }
                            self.filldetail()
                        } catch {
                            print(error)
                            presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                            return
                        }
                        break
                    }
                }
            }
            
            self.topAnchor?.isActive = false
            self.bottomAnchor?.isActive = true
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            isDetailShow = true
        }
    }
    
    fileprivate func filldetail() {
        DispatchQueue.main.async {
            self.detailNameTextView.text = self.detailedShop.shopName
            self.detailAddressTextView.text = self.detailedShop.address
            self.detailOpeningHoursTextView.text = self.detailedShop.openingHours
        }
        
        if detailedCompany.circleImage != nil {
            if let url = URL(string: detailedCompany.circleImage!) {
                let data = try! Data(contentsOf: url)
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.detailLogoImageView.image = image
                }
            }
        }
    }
    
    @IBAction func hideDetail(_ sender: Any) {
        self.topAnchor?.isActive = true
        self.bottomAnchor?.isActive = false
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.isDetailShow = false
        }
    }
    
    @IBAction func showMoreInfoTapped(_ sender: Any) {
        let VC = getShopVC()
        //VC.shop = detailedShop.shop
        //VC.bonus = detailedShop.bonus
        VC.companyId = detailedCompany.companyId
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //guard let curLocation = manager.location?.coordinate else {return}
        
        if let coordinate = manager.location?.coordinate {
            if firstStart {
                let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
                self.mapView.setRegion(region, animated: true)
                firstStart = false
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Can't get your location \(error)")
    }
}

extension HomeViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.title == "Вы здесь!" {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        annotationView.image = UIImage(named: "Annotation")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation?.title == "Вы здесь!" { return }
        if let coordinate = view.annotation?.coordinate {
            showDetail(coordinate: coordinate)
        }
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCollectionViewCell {
            itemCell.fill(item: categories[indexPath.row])
            return itemCell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateMapAnnotations(categoryId: categories[indexPath.row].id)
    }
}
