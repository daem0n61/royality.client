//
//  QRViewController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 30/11/2018.
//  Copyright © 2018 Дмитрий Жданов. All rights reserved.
//

import UIKit

class QRViewController: UIViewController {
    @IBOutlet weak var qrImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        DispatchQueue.global(qos: .utility).async {
            if let walletId = UserDefaults.standard.string(forKey: "WalletId") {
                let data = walletId.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
                let filter = CIFilter(name: "CIQRCodeGenerator")!
                filter.setValue(data, forKey: "inputMessage")
                filter.setValue("Q", forKey: "inputCorrectionLevel")
                let qrCodeImage = filter.outputImage!
                
                DispatchQueue.main.async {
                    let scale = self.qrImageView.frame.size.width / qrCodeImage.extent.size.width
                    let transformedImage = qrCodeImage.transformed(by: CGAffineTransform(scaleX: scale, y: scale))
                    self.qrImageView.image = UIImage(ciImage: transformedImage)
                }
            }
            else {
                presentAllert(VC: self, message: "Нужно войти в аккаунт хоть раз") 
            }
        }
    }
}
