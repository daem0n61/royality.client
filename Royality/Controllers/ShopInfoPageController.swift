//
//  ShopInfoPageController.swift
//  Royality
//
//  Created by Дмитрий Жданов on 02/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class ShopInfoPageController: UIPageViewController, UIPageViewControllerDataSource {
    var shopVCs = [ShopInfoController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
    }
    
    func initialize(shops: [Shop], delegate: ShopViewController) {
        for shop in shops {
            let VC = getShopInfoVC()
            VC.shop = shop
            VC.delegate = delegate
            shopVCs.append(VC)
        }
        
        setViewControllers([shopVCs.first!], direction: .forward, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = shopVCs.index(of: viewController as! ShopInfoController) {
            return index > 0 ? shopVCs[index - 1] : nil
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = shopVCs.index(of: viewController as! ShopInfoController) {
            return index < shopVCs.count - 1 ? shopVCs[index + 1] : nil
        } else {
            return nil
        }
    }
}
